# Teste Programador Front-End Responsivo

* 1) Fazer uma Branche desse repositório com seu nome Ex: jose-da-silva;

* 2) Criar uma tela de login;

* 3) Criar uma tela de listagem de usuários;

* 4) Ao finalizar solicitar um Pull-Request;

* 5) Enviar um E-mail de finalização;

---

# O Objetivo do teste:


Avaliar usas habilidades com GIT, HTML5, CSS3, JQuery e Javascript

---

# Consumindo todos um serviço rest json

* No entry point de Login:

	POST: https://go-rest-mysql.herokuapp.com/api_login/login


* Campos enviados:

	* "ch_email" : "hermes.canuto@gmail.com",
	* "ch_senha" : "1234"

---

* No entry point de Usuários:

	GET: https://go-rest-mysql.herokuapp.com/api_usuario/usuario

	Retorna todos os usuario

---

* No entry point de detalhes Usuário:
	
	GET: https://go-rest-mysql.herokuapp.com/api_usuario/usuario/1 
	retorna o usuario com id =1
	
---